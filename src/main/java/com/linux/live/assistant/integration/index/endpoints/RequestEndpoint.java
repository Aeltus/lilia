package com.linux.live.assistant.integration.index.endpoints;

import com.linux.live.assistant.integration.index.services.impl.RequestServiceImpl;
import io.spring.guides.lilia_web_service.GetRequest;
import io.spring.guides.lilia_web_service.GetResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class RequestEndpoint {

    private static Logger LOGGER = LoggerFactory.getLogger(RequestEndpoint.class);

    @Autowired
    RequestServiceImpl requestServices;

    @PayloadRoot(localPart="getRequest", namespace="http://www.linux.live.assistant")
    public @ResponsePayload GetResponse order(@RequestPayload GetRequest request) {

        LOGGER.info("Passe par le debut de endpoint");

        requestServices.launch(request);

        LOGGER.info("Prepare la réponse");

        GetResponse response = new GetResponse();
        response.setContent("toto");

        return response;
    }

}
