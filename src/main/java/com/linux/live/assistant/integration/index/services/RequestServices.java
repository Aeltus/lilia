package com.linux.live.assistant.integration.index.services;

import io.spring.guides.lilia_web_service.GetRequest;
import org.springframework.integration.annotation.Gateway;
import org.springframework.stereotype.Component;

@Component
public interface RequestServices {

    @Gateway
    public GetRequest launch(GetRequest request);

}
