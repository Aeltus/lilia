package com.linux.live.assistant.integration.index.services.impl;

import com.linux.live.assistant.integration.index.services.RequestServices;
import io.spring.guides.lilia_web_service.GetRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RequestServiceImpl implements RequestServices {

    private static Logger LOGGER = LoggerFactory.getLogger(RequestServiceImpl.class);

    @Override
    public GetRequest launch(GetRequest request) {
        LOGGER.info("Request service lancé.");
        return request;
    }
}
