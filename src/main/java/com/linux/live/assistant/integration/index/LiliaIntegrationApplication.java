package com.linux.live.assistant.integration.index;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ComponentScan("com.linux.live.assistant.integration")
@ImportResource("classpath:integration-config.xml")
public class LiliaIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiliaIntegrationApplication.class, args);
	}
}
